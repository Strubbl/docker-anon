FROM node:latest
LABEL maintainer="Strubbl-Dockerfile@linux4tw.de"

RUN git clone https://github.com/edsu/anon.git /anon && cd anon && npm install
VOLUME ["/data"]
ENTRYPOINT ["/anon/anon.js"]
CMD ["--config", "/data/config.json"]
